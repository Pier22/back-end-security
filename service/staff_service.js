const { v4: uuidv4 } = require('uuid');
const db = require('../models/connection');
const staff = db.staff;
const crypto = require('bcrypt');


/**
 * @param {req,res} '\createStaff'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * @param {} data
 * funzione asincrona di create Staff: crea personale role staff
 */
async function createStaff(req, res, data) {

    const existingStaff = await staff.findOne({
        where: {
            Username: data.username
        }
    });

    if (!existingStaff) {
        const passwordSH = crypto.createHash('sha512').update(data.password).digest('hex');
        try {
            return await staff.create({
                UuidStaff: uuidv4(),
                Username: data.username,
                Password: passwordSH,
            });

        } catch (err) {
            console.log(err);
        }
    }
}

/**
 * @param {req,res} '\showStaffByUsername'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * @param {} username INPUT
 * funzione asincrona di show Staff By Username: mostra Staff per nome utente role staff
 */
async function showStaffByUsername(req, res, username) {
    return staff.findOne({
        where: {
            Username: username
        }

    }).then((data) => {
        res.send(data);
    });

}

/**
 * @param {req,res} '\getStaffByCredential'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * @param {} data
 * funzione asincrona di get Staff By Credential: ottenere personale per credenziale role staff
 */
async function getStaffByCredential(req, res, data) {
    let loginStaff;
    const saltRounds = 9;

    loginStaff = await staff.findOne({ where: { Username: data.username } }).then(staff => {
        if (!staff) {
            return false;
        }
        loginStaff = staff;
        console.log("data",data.password)
        console.log("staff",staff.Password)
        const passwordSH = crypto.hashSync(data.password, saltRounds)
        validPas= crypto.compareSync(data.password, staff.Password)
        console.log(validPas)
        if(validPas===true){
            console.log("comapra",passwordSH.localeCompare(staff.Password))
            return passwordSH.localeCompare(staff.Password);
            
        }

    }).then(isEqual => {
        if (isEqual !== 1) {
            return loginStaff = false;
        } else
            return loginStaff;
    });
    return loginStaff;
}



module.exports = {
    getStaffByCredential,
    showStaffByUsername,
    createStaff
};