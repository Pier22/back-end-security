const { v4: uuidv4 } = require('uuid');
const db = require('../models/connection');
const clients = db.client;
const play = db.play;
const crypto = require('bcrypt');
const Sequelize = require("sequelize");
const config = require("../config");
const { QueryTypes } = require('sequelize');

/**
 * @param {req,res} '\createUser'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * @param {} data INPUT
 * funzione asincrona di create User: creare un utente role client
 */
async function createUser(req, res, data) {

    const existingEmail = await clients.findOne({
        where: {
            Email: data.email,
        }
    });

    const existingUsername = await clients.findOne({
        where: {
            Username: data.username
        }
    })

    if (!existingEmail && !existingUsername) {
        const passwordSH = crypto.createHash('sha512').update(data.password).digest('hex');

        try {
            return await clients.create({
                UuidClient: uuidv4(),
                Username: data.username,
                Email: data.email,
                Password: passwordSH,
                Name: '',
                Surname: '',
                SerialNumber: ''
            }).then((res) => {
                return play.create({
                    UuidClient: res.UuidClient,
                    UuidRole: data.role
                })
            });

        } catch (err) {
            console.log(err);
        }
    }

}


/**
 * @param {req,res} '\showUserByEmail'
 * @param {req} function(req)
 * @param {email} string email utente
 * @param {}res.sendStatus(200)
 * @param {} email
 * funzione asincrona di show User By Email: mostra l'utente per email role client
 */
async function showUserByEmail(req, res, email) {
    return clients.findOne({
        where: {
            Email: email
        },
        include: [{
            model: db.role,
        }],
    }).then((data) => {
        res.send(data);
    });

}



/**
 * @param {req,res} '\getUserByCredential'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * @param {} data INPUT
 * funzione asincrona di getUser By Credential: Ottieni utente per credenziale role client
 */
async function getUserByCredential(req, res, data) {
    let loginUser;
    const saltRounds = 9;
    
    loginUser = await clients.findOne({ where: { email: data.email } }).then(client => {
        if (!client) {
            return false;
        }
        loginUser = client;
        const passwordSH = crypto.hashSync(data.password, saltRounds)
        validPas= crypto.compareSync(data.password, client.Password)
        
        if(validPas===true){
            return passwordSH.localeCompare(client.Password);
        }

    }).then(isEqual => {
        if (isEqual !== 1) {
            return loginUser = false;
        } else
            return loginUser;
    });
    return loginUser;
}




module.exports = {
    getUserByCredential,
    showUserByEmail,
    createUser,
  
};