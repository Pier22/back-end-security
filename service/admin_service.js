const { v4: uuidv4 } = require('uuid');
const db = require('../models/connection');
const staff = db.staff;
const crypto = require('bcrypt');
const Sequelize = require("sequelize");
const config = require("../config");
const { QueryTypes } = require('sequelize');


/**
 * @param {req,res} '\getAdminByCredential'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * @param {} data INPUT
 * funzione asincrona di get Admin By Credential: ottiene le credenziali di amministratore role admin
 */
async function getAdminByCredential(req, res, data) {
    let loginAdmin;
    const saltRounds = 9;
    loginAdmin = await staff.findOne({ where: { Username: data.username } }).then(staff => {
        if (!staff) {
            return false;
        }
        loginAdmin = staff;
        const passwordSH = crypto.hashSync(data.password, saltRounds)
        validPas= crypto.compareSync(data.password, staff.Password)
        
        if(validPas===true){
            return passwordSH.localeCompare(staff.Password);
        }
        //next operation -> perche' è mappato nella stessa table del DB
    }).then(isEqual => {
        if (isEqual !== 1) {
            return loginAdmin = false;
        } else if (loginAdmin.CanAdmin == true) {
            return loginAdmin;
        }
        console.log(loginAdmin.CanAdmin)
            
    });
    return loginAdmin;  
}


module.exports = {
    getAdminByCredential,

};