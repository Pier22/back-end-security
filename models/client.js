module.exports = (sequelize, Sequelize) => {


    const Client = sequelize.define('clients', {
        UuidClient: {
            type: Sequelize.UUID,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        SerialNumber: {
            type: Sequelize.STRING,
            allowNull: true
        },
        Name: {
            type: Sequelize.STRING,
            allowNull: true
        },
        Surname: {
            type: Sequelize.STRING,
            allowNull: true
        },
        Password: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        Email: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        Username: {
            type: Sequelize.STRING,
            allowNull: false
        },
        UrlImage: {
            type: Sequelize.STRING,
            allowNull: true
        }
    });
    return Client;
};