const db = require("../models/connection");
module.exports = (sequelize, Sequelize) => {
    const Occupy = sequelize.define('occupy', {
        UuidOccupy: {
            type: Sequelize.UUID,
            primaryKey: true
        },
        Date: {
            type: Sequelize.DATEONLY,
            //allowNull: false
        },
        Hour: {
            type: Sequelize.TIME,
            //allowNull: false
        },

    }, {freezeTableName: true});

    return Occupy;
};