require('dotenv').config()
const Logger = require('./logger/logger_service');
const winston_logger = new Logger('app');

module.exports = {
    env: process.env.NODE_ENV,
    JWT_SECRET: process.env.JWT_SECRET,
    DB_NAME: process.env.DB_NAME,
    DB_HOST: process.env.DB_HOST,
    DB_ACCESS: process.env.DB_ACCESS,
    logger: winston_logger,
}