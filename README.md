# UniBookBar back-end Security 

Back-end repository - UniBookBar Project : https://gitlab.com/Pier22/back-end-security
 
############################# PJ_FS
                    
# Piergiuseppe Di Pilla & Francesco Salzano

############################   PJ_FS

#
**ABSTRACT**

Il lavoro svolto nel progetto si incentra sulle corrette pratiche di sviluppo di un prodotto software, tenendo conto dell'intero ciclo di vita, col fine di rispettare i giusti criteri di sicurezza, in accordo con le norme vigenti in materia di protezione dei dati e con le linee guida fornite dall'Agenzia per l'Italia digitale, andando a coinvolgere una serie di tecniche e di tool, affermati come standard de facto in tale ambito. Verrà mostrato come a partire dalla fase di design di un prodotto, coinvolgendo determinate tecniche, quali modellazione e analisi delle minacce, si arrivi ad un buon risultato sotto l'aspetto della qualità e della sicurezza informatica. Con tali pratiche verrà implementato un sistema, avente le stesse funzioni di un sistema esistente sviluppato senza considerare queste tecniche, i due sistemi saranno sottoposti ad un confronto, tramite attacchi e analisi statica tramite il tool SonarQube.

# 
**General_link:** https://gitlab.com/Pier22/back-end-security/-/wikis/Organizzazione

**WIKI:** https://gitlab.com/Pier22/back-end-security/-/wikis/home


                                                 UniBookBar-SecurityPRJ                               
