const rateLimit = require("express-rate-limit");

/**LIMITER **/
// Utilizzato nelle api di login per evitare BruteForce
const apiLimiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 100,
  message:{
    code:429,
    message:'Too many request, now wait'
  }
});

module.exports = {
    apiLimiter,
};