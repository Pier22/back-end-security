const jwt = require('../jwt/jwt');
const { getUserByCredential, createUser,getUser } = require('../service/client_service');
const db = require('../models/connection')
const crypto = require('bcrypt');
const Sequelize = require("sequelize");
const config = require("../config");
const { QueryTypes } = require('sequelize');
var validator = require('validator');

/** CHIAMATE ALLE FUNZIONALITA CLIENTE **/

/**@summary REGISTRAZIONE (CLIENTE)
 * @param {req,res} '\registration'
 * @param {req} function(req)
 * @return {200} res.sendStatus(200) operazione effettuata (registrazione cliente)
 * @return {400} res.sendStatus(400) qualcosa è andato storto
 * @param {} data INPUT
 * funzione asincrona di registration: registazione role client
 */
async function registration(req, res, data) {
    const client = await createUser(req, res, data);

    if (client) {
        const token = jwt.generateTokenClient(client);
        return res.status(200).json({
            message: 'registrato',
            token: token,
            code: 200
        });
    } else {
        res.status(400).json({
            message: 'qualcosa è andato storto',
        });
    }
}

/**@summary MOSTRA PIATTI PER CATEGORIA (CLIENTE)
 * @param {req,res} '\getAllDishesByCategory'
 * @param {req} function(req)
 * @param {uuid_category} string id categoria
 * @return {200} res.sendStatus(200) file json contente i piatti filtrati per categoria (piatti per categoria)
 * funzione asincrona di get All Dishes By Category:200 OK  Ottieni tutti i piatti per categoria role client
 */
async function getAllDishesByCategory(req, res) {
    db.belong.findAll({
        attributes: ['UuidDish', 'UuidCategory'],
        include: {
            model: db.dish,
            attributes: ['Name', 'Price', 'Unit', 'UrlImage']
        }
    }).then((dish) => res.status(200).json(dish));
}

/**@summary LOGIN (CLIENTE)
 * @param {req,res} '\login'
 * @param {req} function(req)
 * @return {200} res.sendStatus(200) autenticato
 * @return {401} res.sendStatus(401) non autorizzato credenziali errate (login)
 * @param {} data INPUT
 * funzione asincrona di login: 200 OK Accesso role client
 */
async function login(req, res, data) {
    const client = await getUserByCredential(req, res, data);
    if (!client) {
        return res.status(401).json({
            message: 'Non autorizzato, credenziali errate',
        });
    } else {
        const token = jwt.generateTokenClient(client);
        return res.status(200).json({
            message: 'autenticato',
            token: token,
            code: 200
        });
    }
}

/**@summary MOSTRA ACCOUNT (CLIENTE)
 * @param {req,res} '\showAccount'
 * @param {req} function(req)
 * @param {uuid_client} string id cliente
 * @return {200} res.sendStatus(200) file json contente i dati dell'account (mostra account)
 * @param {} client INPUT
 * funzione asincrona di show Account: 200 OK mostra account  role client
 */
async function showAccount(req, res, cliente) {
    return db.client.findOne({
        where: { 'UuidClient': cliente },
        attributes: ['Name', 'Surname', 'SerialNumber', 'Username', 'Password', 'UrlImage']
    }).then((user) => res.status(200).send(user));
}

/**@summary MODIFICA ACCOUNT (CLIENTE)
 * @param {req,res} '\editAccount'
 * @param {req} function(req)
 * @param {name} string nome utente
 * @param {surname} string cognome utente
 * @param {password} string password utente
 * @param {url_image} string image
 * @return {200} res.sendStatus(200) operazione effettuata (modifica account)
 * @return {400} res.sendStatus(400) contenuto vuoto
 * @return {403} res.sendStatus(400) password corta
 * @param {} client INPUT
 * funzione asincrona di edit Account: 200 OK modifica account role client
 */
async function editAccount(req, res, client) {
    const { name, surname, username, password, url_image } = req.body;
    
    if (!req || !username) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }

    const c = await db.client.findByPk(client);
    if (name) {
        c.Name = name;
    }

    if (surname) {
        c.Surname = surname;
    }

    // una buona pratica è quella di far inserire password di almeno 8 caratteri per test abilitiamo 5 caratteri : (prova)
    if (password) {
        if (password.length < 5) {
            return res.status(403).json({
                message: "password troppo corta",
            });
        }
        const saltRounds = 9;
        c.Password = crypto.hashSync(password, saltRounds);
    }

    if (url_image) {
        c.UrlImage = url_image;
    }

    c.Username = username;

    c.save().then(() => res.status(200).json({
        message: "operazione effettuata",
        code: 200
    }))
}

/**@summary MOSTRA REVIEW VERSIONE CORRETTA (ADMIN)
 * @param {req,res} '\showReviewByTitle'
 * @param {req} function(req)
 * @returns {200} res.sendStatus(200) mostra le recensioni
 * funzione asincrona di show Review : 200 OK mostra recensione role client
 */
async function showReview(req, res) {
    const { title } = req.query;
    

   if (!title) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    titleS=validator.blacklist(title, "/'|)");

    return db.review.findAll({
        where: { 'Title': titleS },
         attributes: ['Title', 'Text', 'Star']
         
    }).then((final) => {
        res.json(final);
    });
}

module.exports = {
    registration,
    login,
    showAccount,
    editAccount,
    getAllDishesByCategory,
    showReview,
};