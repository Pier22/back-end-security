const db = require("../models/connection");
const { v4: uuidv4 } = require('uuid');

/** CHIAMATE A CATEGORY **/

/**@summary CREA UNA NUOVA CATEGORIA (ADMIN)
 * @param {req,res} '\newCategory'
 * @param {req} function(req)
 * @param {name} string nome categoria
 * @return {200} res.sendStatus(200) operazione effettuata (nuova categoria)
 * @return {400} res.sendStatus(400) contenuto vuoto
 * funzione asincrona di new Category : 200 OK crea nuova categoria
 */
async function newCategory(req, res) {
    const { name } = req.body;
    if (!name) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }
    db.category.create({
        UuidCategory: uuidv4(),
        CategoryName: name
    }).then(res.sendStatus(200));
}


/**@summary ELIMINA CATEGORIA (ADMIN)
 * @param {req,res} '\deleteCategory'
 * @param {req} function(req)
 * @param {uuid_category} string id categoria
 * @return {200} res.sendStatus(200) operazione effettuata (elimina categoria)
 * @return {400} res.sendStatus(400) contenuto vuoto
 * funzione asincrona di delete Category: 200 OK elimina categoria
 */
async function deleteCategory(req, res) {
    const { uuid_category } = req.body;
    if (!uuid_category) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }
    const category = db.category.findByPk(uuid_category);
    category.destroy().then(res.sendStatus(200));
}


/**@summary RINOMINA CATEGORIA (ADMIN)
 * @param {req,res} '\renameCategory'
 * @param {req} function(req)
 * @param {uudi_category} string id categoria
 * @param {name} string nome categoria
 * @return {200} res.sendStatus(200) operazione effettuata (rename categoria)
 * funzione asincrona di rename Category: 200 OK rinomina categoria
 */
async function renameCategory(req, res) {
    const { uudi_category, name } = req.body;
    const category = db.category.findByPk(uudi_category);
    category.Name = name;
    category.save().then(res.sendStatus(200));
}


/**@summary MOSTRA TUTTE LE CATEGORIE (ADMIN)
 * @param {req,res} '\getAllCategories'
 * @param {req} function(req)
 * @return {200} res.sendStatus(200) operazione effettuata (mostra categorie)
 * funzione asincrona di get All Categories : 200 OK ottieni tutte le categorie
 */
async function getAllCategories(req, res) { //cambiare nome
    const category = db.category.findAll({
        attributes: ['CategoryName', 'UuidCategory']
    }).then((category) => res.status(200).json(category));
}

module.exports = {
    getAllCategories,
    newCategory
}