const db = require("../models/connection");
const { v4: uuidv4 } = require('uuid');

/** FUNZIONALITA' PIATTI**/

/**@summary CREA PIATTO (AMDIN, STAFF)
 * @param {req,res} '\createDish'
 * @param {req} function(req)
 * @param {name} string nome piatto
 * @param {price} number prezzo piatto
 * @param {category} string categoria piatto
 * @param {url_image} string image
 * @param {[ingredients]]} arrayString uuid_ingredienti 0 to N 
 * @param {[allergen]]} arrayString uuid_allergen 0 to N 
 * @return {200} res.sendStatus(200) opreazione effettuata (creazione piatto)
 * @return {400} res.sendStatus(400) contenuto vuoto
 * @param {} staff INPUT
 * funzione asincrona di create Dish: 200 OK crea piatto role staff e admin
 */
async function createDish(req, res, staff) {
    const { name, price, category, url_image, bkble_conble, ingredients, allergens } = req.body;
    if (!req || !name || bkble_conble.isEmpty || !price || !url_image || !category) {

        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    await db.dish.create({
        UuidDish: uuidv4(),
        Name: name,
        Price: price,
        BkbleConble: bkble_conble,
        UrlImage: url_image
    }).then((dish) => {
        for (const ingredient of ingredients) {
            db.made.create({
                UuidDish: dish.UuidDish,
                UuidIngredient: ingredient
            });
        }
        return dish;
    }).then((dish) => {
        for (const allergen of allergens) {
            db.content.create({
                UuidDish: dish.UuidDish,
                UuidAllergen: allergen
            });
        }
        return dish;
    }).then((dish) => {
        db.belong.create({
            UuidDish: dish.UuidDish,
            UuidCategory: category
        })
        return dish;
    }).then((dish) => {
        console.log(staff);
        db.manage.create({
            UuidManage: uuidv4(),
            UuidDish: dish.UuidDish,
            UuidStaff: staff,
            Azione: "creazione Piatto"
        })
    }).then(() => res.status(200).json({ message: "operazione effettuata" }));
}


/**@summary MOSTRA TUTTI I PIATTI  (AMDIN, STAFF)
 * @param {req,res} '\showAllDishes'
 * @param {req} function(req)
 * @return {200} res.sendStatus(200) file json di ritorno contente i piatti
 * funzione asincrona di show All Dishes: 200 Ok  mostra tutti i piatti role admin e staff
 */
async function showAllDishes(req, res) {
    db.dish.findAll({
        attributes: ['UuidDish', 'Name', 'Price', 'BkbleConble', 'UrlImage']
    }).then((dish) => res.status(200).json(dish));
}


/**@summary MOSTRA TUTTI I PIATTI E LA RISPETTIUVA QUANTITA'  (AMDIN, STAFF)
 * @param {req,res} '\getAllDishes'
 * @param {req} function(req)
 * @return {200} res.sendStatus(200) file json di ritorno contente i piatti e le quantita'(mostra piatti)
 * funzione asincrona di getAllDishes:200 OK mostra tutti i piatti role staff e admin
 */
async function getAllDishes(req, res) {
    db.dish.findAll({
        attributes: ['UuidDish', 'Name', 'Price', 'BkbleConble', 'UrlImage', 'Unit']
    }).then((dish) => res.status(200).json(dish));
}


/**@summary MODIFICA PIATTO (AMDIN, STAFF)
 * @param {req,res} '\editDish'
 * @param {req} function(req)
 * @param {uuid_dish} string id del piatto
 * @param {name} string nome del piatto
 * @param {price} string prezzo del piatto
 * @param {category} string id categoria del piatto
 * @param {url_image} string image
 * @param {[ingredients]} arrayString id dei vari ingredienti
 * @param {[allergens]} arrayString id dei vari allergeni
 * @param {[quantity]} arrayString unit quantita' disponibile
 * @return {200} res.sendStatus(200) operazione effettuata (modifica piatto)
 * @return {400} res.sendStatus(400) contenuto vuoto(modifica piatto)
 * @param {} staff INPUT
 * funzione asincrona di edit Dish: 200 OK modifica piatto staff e admin
 */
async function editDish(req, res, staff) {
    const { uuid_dish, name, price, category, url_image, bkble_conble, ingredients, allergens, quantity } = req.body;
    if (!req || !uuid_dish || !name || !price || !category || !url_image || bkble_conble.isEmpty) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const dish = await db.dish.findByPk(uuid_dish);
    if (!dish) {
        return res.status(400).json({
            message: "piatto non trovato",
        });
    }
    dish.Name = name;
    dish.Price = price;
    dish.UrlImage = url_image;
    dish.BkbleConble = bkble_conble;
    dish.Unit = quantity;
    dish.save();
    await db.belong.findOne({
        where: { 'UuidDish': uuid_dish }
    }).then((cat) => {
        cat.UuidCategory = category;
        cat.save();
    });
    const tmp = await db.made.findAll({
        where: { 'UuidDish': uuid_dish }
    })
    for (const temp of tmp) {
        await temp.destroy();
    }
    for (const ingredient of ingredients) {
        await db.made.create({
            UuidDish: uuid_dish,
            UuidIngredient: ingredient
        });
    }
    const tmpAllergen = await db.content.findAll({
        where: { 'UuidDish': uuid_dish }
    });
    for (const temp of tmpAllergen) {
        await temp.destroy();
    }
    for (const allergen of allergens) {
        await db.content.create({
            UuidDish: uuid_dish,
            UuidAllergen: allergen
        });
    }
    db.manage.create({
        UuidManage: uuidv4(),
        UuidDish: dish.UuidDish,
        UuidStaff: staff,
        Azione: "modifica Piatto"
    })
    res.status(200).json({
        message: "operazione effettuata",
    });
}


/**@summary PIATTI PER CATEGORIA 
 * @param {req,res} '\dishesByCategory'
 * @param {req} function(req)
 * @param {category} string id categoria
 * @return {200} res.sendStatus(200) file json di ritorno contente i piatti per categoria
 * @return {400} res.sendStatus(400) contenuto vuoto
 * @return {403} categoria non trovata
 * funzione asincrona di dishes By Category:200 OK piatti per categoria
 */
async function dishesByCategory(req, res) {
    const { category } = req.query;
    if (!category) {
        return res.status(400).json({

            message: "Il contenuto non può essere vuoto",
        });
    }
    const isPresent = await db.category.findOne({
        where: { UuidCategory: category }
    })
    if (!isPresent) {
        return res.status(403).json({
            message: "Categoria non trovata",
        });
    }
    await db.belong.findAll({
        where: { UuidCategory: category },
        include: {
            model: db.dish,
            attributes: ['Name', 'Price', 'UrlImage']
        }
    }).then((dish) => {
        res.status(200).json(dish)
    })
}


/**@summary MOSTRA DETTAGLI PIATTO (ADMIN,STAFF,CLIENTE)
 * @param {req,res} '\showDetailDish'
 * @param {req} function(req)
 * @param {uuid_dish} string id del piatto
 * @return {400} res.sendStatus(400) contenuto vuoto
 * @return {200} res.sendStatus(200) cfile json di ritorno contente i dettagli del piatto(mostra dettagli piatto)
 * funzione asincrona di show Detail Dish: 200 OK mostra dettaglio piatto
 */
async function showDetailDish(req, res) {
    const { uuid_dish } = req.query;
    if (!uuid_dish) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const dish = await db.dish.findOne({
        where: { UuidDish: uuid_dish },
        attributes: ['Name', 'Urlimage']
    })
    if (!dish) {
        return res.status(400).json({
            message: "Il piatto non è stato trovato",
        });
    }
    const ingredient = await db.made.findAll({
        where: { UuidDish: uuid_dish },
        include: {
            model: db.ingredient,
            attributes: ['Name']
        }
    });
    const allergens = await db.content.findAll({
        where: { UuidDish: uuid_dish },
        include: {
            model: db.allergen,
            attributes: ['Name']
        }
    });
    const mediaRecensioni = await starAvg(uuid_dish);
    const json = { dish, ingredient, allergens, mediaRecensioni }
    res.status(200).json(json);
}


/**@summary STELLE RECENSIONE (CLIENTE)
 * '\starAvg'
 * @param {uuid_dish} string id del piatto
 * funzione asincrona di star Avg
 */
async function starAvg(uuid_dish) {
    const review = await db.references.findAll({
        where: { UuidDish: uuid_dish },
        include: {
            model: db.review,
            attributes: ['Star']
        }
    });
    var tot = 0;
    var i = 0;
    for (const rev of review) {
        tot = rev.review.Star + tot;
        i = i + 1;
    }
    return [tot / i, i];
}


/**@summary ELIMINA PIATTO (ADMIN, STAFF)
 * @param {req,res} '\deleteDish'
 * @param {req} function(req)
 * @param {uuid_dish} string id del piatto da eliminare
 * @return {200} res.sendStatus(200) operazione effettuata (elimina piatto)
 * @return {400} res.sendStatus(400) contentuo vuoto
 * @return {404} res.sendStatus(404) piatto non trovato
 * funzione asincrona di delete Dish:200 OK elimina piatto
 */
async function deleteDish(req, res) {
    const { uuid_dish } = req.body;
    if (!uuid_dish) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const dish = await db.dish.findByPk(uuid_dish)
    if (!dish) {
        return res.status(404).json({
            message: "piatto non trovato",
        });
    }
    const reference = await db.references.findAll({
        where: { UuidDish: uuid_dish },
        attributes: ['UuidRev']
    })
    for (const ref of reference) {
        const rev = await db.review.findByPk(ref.UuidRev)
        await rev.destroy();
        await ref.destroy();
    }
    await dish.destroy().then(() => res.status(200).json({
        message: "operazione effettuata",
    }));
}


/**@summary MOSTRA PIATTO (CLIENTE)
 * @param {req,res} '\showDish'
 * @param {req} function(req)
 * @param {uuid_dish} string id del piatto da mostare
 * @return {200} res.sendStatus(200) file json contente il piatto (mostra piatto)
 * @return {400} res.sendStatus(400) contenuto vuoto
 * funzione asincrona di show Dish: 200 OK mostra piatto
 */
async function showDish(req, res) {
    const { uuid_dish } = req.query;
    if (!uuid_dish) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const dish = await db.dish.findOne({
        where: { UuidDish: uuid_dish },
        attributes: ['Name', 'Urlimage', 'BkbleConble', 'Price']
    })
    if (!dish) {
        return res.status(400).json({
            message: "il piatto non è stato trovato",
        });
    }
    const ingredient = await db.made.findAll({
        where: { UuidDish: uuid_dish },
        include: {
            model: db.ingredient,
            attributes: ['UuidIngredient', 'Name']
        }
    });
    const allergens = await db.content.findAll({
        where: { UuidDish: uuid_dish },
        include: {
            model: db.allergen,
            attributes: ['UuidAllergen', 'Name']
        }
    });
    const category = await db.belong.findOne({
        where: { UuidDish: uuid_dish },
        include: {
            model: db.category,
            attributes: ['UuidCategory', 'CategoryName']
        }
    })
    var Ingredients = [];
    var Allergens = [];
    var i = 0;
    var j = 0;
    for (const ing of ingredient) {
        Ingredients[i] = ingredient[i]['Ingredient']
        i = i + 1;
    }
    for (const aller of allergens) {
        Allergens[j] = allergens[j]['Allergen']
        j = j + 1;
    }

    let Dish = {
        nameDish: dish['Name'],
        BkbleConble: dish['BkbleConble'],
        Price: dish['Price'],
        Urlimage: dish['Urlimage'],
        ingredient: Ingredients,
        allergens: Allergens,
        category: category['Category']
    }

    const json = { Dish }

    res.status(200).json(json);
}


/**@summary MOSTRA PIATTO PER ID(CLIENTE)
 * @param {req,res} '\getDish'
 * @param {req} function(req)
 * @param {uuid_dish} string id del piatto 
 * @return {200} res.sendStatus(200) file json contente i campi del piatto (mostra piatto)
 * @return {400} res.sendStatus(400) contenuto vuoto
 * funzione asincrona di get Dish: 200 OK dati di un piatto
 */
async function getDish(req, res) {
    const { uuid_dish } = req.query;
    if (!uuid_dish) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const dish = await db.dish.findOne({
        where: { UuidDish: uuid_dish },
        attributes: ['Name', 'Urlimage', 'BkbleConble', 'Price', 'Unit']
    })
    if (!dish) {
        return res.status(400).json({
            message: "il piatto non è stato trovato",
        });
    }
    const ingredient = await db.made.findAll({
        where: { UuidDish: uuid_dish },
        include: {
            model: db.ingredient,
            attributes: ['UuidIngredient', 'Name']
        }
    });
    const allergens = await db.content.findAll({
        where: { UuidDish: uuid_dish },
        include: {
            model: db.allergen,
            attributes: ['UuidAllergen', 'Name']
        }
    });
    const category = await db.belong.findOne({
        where: { UuidDish: uuid_dish },
        include: {
            model: db.category,
            attributes: ['UuidCategory', 'CategoryName']
        }
    })
    var Ingredients = [];
    var Allergens = [];
    var i = 0;
    var j = 0;
    for (const ing of ingredient) {
        Ingredients[i] = ingredient[i]['ingredient']
        i = i + 1;
    }
    for (const aller of allergens) {
        Allergens[j] = allergens[j]['allergen']
        j = j + 1;
    }

    let Dish = {
        nameDish: dish['Name'],
        BkbleConble: dish['BkbleConble'],
        Price: dish['Price'],
        Urlimage: dish['Urlimage'],
        ingredient: Ingredients,
        allergens: Allergens,
        category: category['category'],
        quantity: dish['Unit']
    }

    const json = { Dish }

    res.status(200).json(json);
}


module.exports = {
    createDish,
    showAllDishes,
    editDish,
    dishesByCategory,
    showDetailDish,
    deleteDish,
    showDish,
    getDish,
    getAllDishes
}