const db = require('../models/connection');
const { v4: uuidv4 } = require('uuid');

/** FUNZIONALITA' REVIEW **/

/**@summary SCRIVI RECENSIONE (CLIENTE)
 * @param {req,res} '\writeReview'
 * @param {req} function(req)
 * @param {uuid_dish} string id del piatto
 * @param {text} string testo del messaggio
 * @param {title} string titolo del messaggio
 * @param {star} string stelle piatto riferito al messaggio
 * @return {400} res.sendStatus(400) contenuto vuoto
 * @return {200} res.sendStatus(200) operazione effettuata
 * @param {} client  INPUT
 * funzione asincrona di write Review: 200 OK scrivi una recensione
 */
async function writeReview(req, res, client) {
    const { uuid_dish, text, title, star } = req.body;
    if (!req.body || !uuid_dish || !text || !title || !star) {
        return res.status(400).json({
            message: "contenuto vuoto",
        });
    }
    if (star > 5 || star < 0) {
        return res.status(400).json({
            message: "Il valore delle stelle non è valido",
        });
    }
    const UuidReview = uuidv4();
    return await db.review.create({
        UuidRev: UuidReview,
        Text: text,
        Title: title,
        Star: star
    }).then(() => {
        return db.writing.create({
            UuidRev: UuidReview,
            UuidClient: client,
            Date: Date.now(),
            Hour: getTime()
        })
    }).then(() => {
        return db.references.create({
            UuidRev: UuidReview,
            UuidDish: uuid_dish
        }).then(() => res.status(200).json({
            message: "operazione effettuata",
            code: 200
        }))
    })
}


/**@summary MOSTRA RECENSIONI (CLIENTE)
 * @param {req,res} '\showMyReviews'
 * @param {req} function(req)
 * @return {200} res.sendStatus(200) file json di ritorno con i dati della review
 * @param {} client INPUT
 * funzione asincrona di show My Reviews:200 OK  mostra le miei recensioni
 */
async function showMyReviews(req, res, client) {
    return db.writing.findAll({
        where: {
            UuidClient: client
        },
        include: {
            model: db.review,
            attributes: ['Title', 'Text', 'star']
        }
    }).then((review) => {
        res.status(200).json(review)
    });
}


/**@summary ELIMINA RECENSIONE (CLIENTE)
 * @param {req,res} '\deleteReview'
 * @param {req} function(req)
 * @param {uuid_rev} id della recensione da eliminare
 * @return {200} res.sendStatus(200) operazione effettuata
 * @return {400} res.sendStatus(400) Il contenuto non puo essere vuoto
 * funzione asincrona di delete Review:200 OK elimina recensione
 */
async function deleteReview(req, res) {
    const { uuid_rev } = req.body;
    if (!uuid_rev) {
        return res.status(400).json({
            message: "Il contenuto non puo essere vuoto",
        });
    }
    const review = await db.review.findOne({
        where: { UuidRev: uuid_rev }
    });
    if (!review) {
        return res.status(400).json({
            message: "La recensione non esiste",
        });
    }
    review.destroy().then(() => {
        res.status(200).json({
            message: "operazione effettuata",
            code: 200
        });
    });
}

/** GET TIME
 * @param {} '\getTime'
 * ritorna il tempo
 **/
function getTime() {
    const d = Date(Date.now());
    const time = d.split(" ")
    return time[4];
}

module.exports = {
    writeReview,
    showMyReviews,
    deleteReview
}