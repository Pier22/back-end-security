const db = require("../models/connection");
const { v4: uuidv4 } = require('uuid');

/** FUNZIONALITA' ROLE **/

/**@summary CREA RUOLO
 * @param {req,res} '\newRole'
 * @param {req} function(req)
 * @param {name} string nome ruolo
 * @return {200} res.sendStatus(200) operazione effettauta
 * funzione asincrona di newRole:200 OK nuovo ruolo role
 */
async function newRole(req, res) {
    const { name } = req.body;
    db.role.create({
        UuidRole: uuidv4(),
        NameRole: name
    }).then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}


/**@summary ELIMINA RUOLO
 * @param {req,res} '\deleteRole'
 * @param {req} function(req)
 * @param {uuid_role} string id ruolo da rimuovere
 * @return {200} res.sendStatus(200) operazione effettuata
 * funzione asincrona di delete Role: 200 OK elimina ruolo role
 */
async function deleteRole(req, res) {
    const { uuid_role } = req.body;
    const role = await db.ingredient.findByPk(uuid_ingredient);
    role.destroy().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}


/**@summary RINOMINA RUOLI
 * @param {req,res} '\renameRole'
 * @param {req} function(req)
 * @param {uuid_role} string id del ruolo da rinominare
 * @return {200} res.sendStatus(200) operazione effettauta
 * funzione asincrona di rename Role: 200 OK rinomina ruolo role
 */
async function renameRole(req, res) {
    const { uuid_role, name } = req.body;
    const role = await db.ingredient.findByPk(uuid_ingredient);
    role.Name = name;
    role.save().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}


/**@summary MOSTRA TUTTI I TUOLI
 * @param {req,res} '\getAllRole'
 * @param {req} function(req)
 * @return {200} res.sendStatus(200) file json contente i ruoli
 * funzione asincrona di get All Role: 200 OK ottieni tutti i ruoli role
 */
async function getAllRole(req, res) {
    const role = await db.role.findAll({
        attributes: ['NameRole', 'UuidRole']
    }).then((role) => res.status(200).json({ role }));
}

module.exports = {
    getAllRole,
    newRole
}