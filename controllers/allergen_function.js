const db = require("../models/connection");
const { v4: uuidv4 } = require('uuid');

/** CHIAMATE AD ALLERGENI **/

/**@summary CREA NUOVO ALLERGENE (ADMIN)
 * @param {req,res} '\newAllergeny'
 * @param {req} function(req)
 * @param {name} string nome allergene
 * @return {200} res.sendStatus(200) operazione effettuata (nuovo allergene)
 * @return {400} res.sendStatus(400) contenuto vuoto 
 * @return {403} res.sendStatus(403) allergene gia' presente
 * funzione asincrona di new Allergen : 200 OK nuovo allergene role admin 
 */
async function newAllergen(req, res) {
    const { name } = req.body;
    if (!name) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }

    const allergenToFind = await db.allergen.findAll({
        where: { Name: name }
    });

    if (allergenToFind.length > 0) {
        return res.status(403).json({

            message: "allergene gia' presente",
        });
    }
    await db.allergen.create({
        UuidAllergen: uuidv4(),
        Name: name
    }).then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}


/**@summary ELIMINA ALLERGENE (ADMIN)
 * @param {req,res} '\deleteAllergen'
 * @param {req} function(req)
 * @param {uuid_allergen} string id identifica allergene
 * @return {200} res.sendStatus(200) operazione effettuata (elimina allergene)
 * @return {400} res.sendStatus(400) contenuto vuoto 
 * funzione asincrona di delete Allergen : elimina allergene role admin
 */
async function deleteAllergen(req, res) {
    const { uuid_allergen } = req.body;
    if (!uuid_allergen) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }
    const allergen = await db.allergen.findByPk(uuid_allergen);
    allergen.destroy().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}



/**@summary RINOMINA ALLERGENE (ADMIN)
 * @param {req,res} '\renameAllergen'
 * @param {req} function(req)
 * @param {uuid_allergen} string id identifica allergene
 * @param {name} string nomeallergene
 * @return {200} res.sendStatus(200) operazione effettuata (rinomina allergene)
 * @return {400} res.sendStatus(400) contenuto vuoto 
 * @return {403} res.sendStatus(400) alleregene gia' presente
 * funzione asincrona di rename Allergen : 200 OK rinimina allergene role admin
 */
async function renameAllergen(req, res) {
    const { uuid_allergen, name } = req.body;
    if (!uuid_allergen || !name) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }

    const allergenToFind = await db.allergen.findAll({
        where: { Name: name }
    });

    if (allergenToFind.length > 0) {
        return res.status(403).json({

            message: "allergene gia' presente",
        });
    }

    const allergen = await db.allergen.findByPk(uuid_allergen);
    allergen.Name = name;
    allergen.save().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}


/**@summary MOSTRA TUTTI I VARI ALLERGENI (ADMIN)
 * @param {req,res} '\get All Allergens'
 * @param {req} function(req)
 * @return {200} res.sendStatus(200) operazione effettuara (mostra allergeni)
 * funzione asincrona di get All Allergens :200 OK  ottenere tutti gli allergeni
 */
async function getAllAllergens(req, res) {
     db.allergen.findAll({
        attributes: ['Name', 'UuidAllergen']
    }).then((allergens) => res.status(200).json(allergens));
}

module.exports = {
    getAllAllergens,
    newAllergen,
    renameAllergen,
    deleteAllergen
}