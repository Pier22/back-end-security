const db = require("../models/connection");
const { getAdminByCredential, createAdmin, getAdmin } = require('../service/admin_service');
const { v4: uuidv4 } = require('uuid');
const crypto = require('bcrypt');
const jwt = require('../jwt/jwt');
const Sequelize = require("sequelize");
const config = require("../config");
const { QueryTypes } = require('sequelize');
var validator = require('validator');

/** FUNZIONALITA' ADMIN **/

/**@summary MOSTRA TUTTI I CLIENTI (ADMIN)
 * @param {req,res} '\showAllClients'
 * @param {req} function(req)
 * @returns {200} res.sendStatus(200) file json contente tutti i clienti
 * funzione asincrona di show All Clients :200 OK mostra tutti i clienti role admin
 */
async function showAllClients(req, res) {
    return db.play.findAll({
        attributes: ['UuidClient'],
        include: [{
                model: db.role,
                attributes: ['NameRole']
            },
            {
                model: db.client,
                attributes: ['Name', 'Surname', 'Password', 'Username', 'SerialNumber', 'UrlImage'],
            }
        ]
    }).then((client) => {
        res.status(200).json(client);
    });
}

/** login
 * @summary LOGIN-ACCESSO (ADMIN)
 * @param {req,res} 
 * @param {req} function(req)
 * @returns {200} res.sendStatus(200) autenticato
 * @returns {401} res.sendStatus(401) non autorizzato
 * @param {} data
 * funzione asincrona di login : 200 OK accedi come admin role admin
 */
async function login(req, res, data) {
    const admin = await getAdminByCredential(req, res, data);
    if (!admin) {

        return res.status(401).json({
            message: 'Non autorizzato'
        });
    } else {
        const token = jwt.generateTokenStaff(admin);
        return res.status(200).json({
            message: 'autenticato',
            token: token
        });
    }
}

/**@summary ELIMINA UTENTE  (ADMIN)
 * @param {req,res} '\deleteClient'
 * @param {req} function(req)
 * @param {uuid_client} function(req,uuid_client) uuid_client string in input
 * @returns {400} res.sendStatus(400) contenuto vuoto || cliente non trovato
 * @returns {200} res.sendStatus(200) opreazione effettuata (eliminazione utente)
 * funzione asincrona di delete Client : 200 OK elimina cliente role admin
 */
async function deleteClient(req, res) {
    const uuid_client = req.body
    if (!req || !uuid_client) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const id = uuid_client['uuid_client'];
    idS=validator.blacklist(id, "/'|)");
    const client = await db.client.findByPk(idS);
    if (!client) {
        return res.status(400).json({
            message: "cliente non trovato",
        });
    }
    await client.destroy().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}

/**@summary MODIFICA UTENTE  (ADMIN)
 * @param {req,res} '\editClient'
 * @param {req} function(req)
 * @param {uuid_client} string  identifica cliente
 * @param {role}  string  ruolo cliente
 * @param {name}  string nome utente
 * @param {surname}  string cognome utente
 * @param {username}  string username cliente
 * @param {password}  string pws cliente
 * @param {url_image}  string image
 * @returns {400} res.sendStatus(400) contenuto vuoto || cliente non trovato
 * @returns {200} res.sendStatus(200) opreazione effettuata (modifica utente)
 * funzione asincrona di edit Client : 200 OK modifica cliente role admin
 */
async function editClient(req, res) {
    const { uuid_client, role, name, surname, username, password, url_image } = req.body;
    if (!req || !username || !uuid_client || !role || !password) {
        return res.status(400).json({
            message: "il contenuto non può essere vuoto",
        });
    }
    nameS=validator.blacklist(name, "/'|)");
    surnameS=validator.blacklist(surname, "/'|)");
    usernameS=validator.blacklist(username, "/'|)");
    const client = await db.client.findByPk(uuid_client);
    if (!client) {
        return res.status(400).json({
            message: "cliente non trovato",
        });
    }
    const chesckUsername = await db.client.findAll({
        where: { 'Username': username }
    })
    if (chesckUsername.length !== 0) {
        return res.status(400).json({
            message: "Username gia presente",
        });
    }
    const saltRounds = 9;
    const passwordSH = crypto.hashSync(password, saltRounds);
    client.Name = nameS
    client.Surname = surnameS
    client.Username = usernameS
    client.Password = passwordSH
    client.UrlImage = url_image

    try{
    await db.play.update({
        UuidRole: role
    }, {
        where: { 'UuidClient': client.UuidClient }
    })
    client.save().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}catch(e){
    res.status(400).json({
        message: "ruolo non valido"
    });
}}

/**@summary MOSTRA UTENTE  (ADMIN)
 * @param {req,res} '\showClient'
 * @param {req} function(req)
 * @param {uuid_client} string identifica il cliente da moistrare
 * @returns {400} res.sendStatus(400) contenuto vuoto 
 * @returns {200} res.sendStatus(200) file json contente i dati del cliente
 * funzione asincrona di show Client : 200 OK mostra dati cliente role admin
 */
async function showClient(req, res) {
    const uuid_client = req.query
    if (!req || !uuid_client) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const id = uuid_client['uuid_client'];
    idS=validator.blacklist(id, "/'|)");
    try{
    await db.play.findOne({
        attributes: ['UuidClient'],
        where: { 'UuidClient': idS },
        include: [{
                model: db.role,
                attributes: ['UuidRole', 'NameRole']
            },
            {
                model: db.client,
                attributes: ['Name', 'Surname', 'Password', 'Username', 'UrlImage'],
            }
        ]
    }).then((client) => {
        res.json(client);
    });
}catch(e){
    res.status(400).json({
        message: "non valido"
    }); 
}
}


/**@summary CREA UTENTE  (ADMIN)
 * @param {req,res} '\newClient'
 * @param {req} function(req)
 * @param {uuid_client} string  identifica cliente
 * @param {role}  string  ruolo cliente
 * @param {name}  string nome utente
 * @param {surname}  string cognome utente
 * @param {username}  string username cliente
 * @param {password}  string pws cliente
 * @param {url_image}  string image
 * @param {email}  string email cliente
 * @returns {200} res.sendStatus(200) operazione effettuata (creazione cliente)
 * @returns {403} res.sendStatus(403) username o email gia' presenti 
 * @returns {400} res.sendStatus(400) contenuto vuoto
 * funzione asincrona di new Client: 200 OK crea un nuovo utente role admin
 */
async function newClient(req, res) {
    const { role, name, surname, username, password, url_image, email } = req.body;
    const clients = await db.client.findAll()
    for (const client of clients) {
        if (client.Username == username || client.Email == email) {
            return res.status(403).json({
                message: "username o email gia presenti ",
            });
        }
    }
    if (!req || !email || !role || !password) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }

    const saltRounds = 9;
    const passwordSH = crypto.hashSync(password, saltRounds);
    return await db.client.create({
        UuidClient: uuidv4(),
        Password: passwordSH,
        Name: name,
        Surname: surname,
        Username: username,
        UrlImage: url_image,
        Email: email
    }).then((client) => {
        return db.play.create({
            UuidClient: client.UuidClient,
            UuidRole: role
        }).then(() => res.status(200).json({ message: "operazione effettuata" }))
    });
}

/**@summary MOSTRA REVIEW  (ADMIN)
 * @param {req,res} '\showReview'
 * @param {req} function(req)
 * @returns {200} res.sendStatus(200) mostra le recensioni
 * funzione asincrona di show Review : 200 OK mostra recensione role admin
 */
async function showReview(req, res) {
    return db.writing.findAll({
        include: [{
                model: db.review,
                attributes: ['Title', 'Text', 'Star']
            },
            {
                model: db.client,
                attributes: ['Username', 'UrlImage']
            }
        ]
    }).then((reservation) => {
        res.json(reservation);
    });
}

/**@summary CREA NUOVO STAFF (ADMIN)
 * @param {req,res} '\newStaff'
 * @param {req} function(req)
 * @param {name}  string nome staff
 * @param {surname}  string cognome staff
 * @param {username}  string username staff
 * @param {password}  string pswstaff
 * @returns {200} res.sendStatus(200) operazione effettuata (creazione staff)
 * @returns {400} res.sendStatus(400) contenuto vuoto
 * funzione asincrona di new Staff : 200 OK nuovo personale role admin
 */
async function newStaff(req, res) {
    const { name, surname, username, password } = req.body;
    const staffs = await db.staff.findAll()
    for (const staff of staffs) {
        if (staff.Username == username) {
            return res.status(403).json({
                message: "username gia presente ",
            });
        }
    }
    if (!req || !username || !password) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const saltRounds = 9;
    const passwordSH = crypto.hashSync(password, saltRounds);
    return await db.staff.create({
        UuidStaff: uuidv4(),
        Password: passwordSH,
        Name: name,
        Surname: surname,
        Username: username,
    }).then(() => res.status(200).json({ message: "operazione effettuata", code: 200 }));
}

/**@summary MOSTRA TUTTI I MEMBRI STAFF (ADMIN)
 * @param {req,res} '\showAllStaff'
 * @param {req} function(req)
 * @returns {200} res.sendStatus(200) file json contentenenti i dati dei membri staff (mostra staff)
 * funzione asincrona di show All Staff :200 OK mostra tutto il personale role admin
 */
async function showAllStaff(req, res) {
    return db.staff.findAll({
        attributes: ['UuidStaff', 'Name', 'Surname', 'Password', 'Username']
    }).then((staff) => {
        res.status(200).json(staff);
    });
}


/**@summary ELIMINA UNO STAFF (ADMIN)
 * @param {req,res} '\deleteStaff'
 * @param {uuid_staff} function(uuid_staff) uuid identifica staff
 * @returns {200} res.sendStatus(200) operazione effettuata (eliminaizone staff)
 * @returns {400} res.sendStatus(400) contenuto vuoto || staff non trovato
 * funzione asincrona di delete Staff :200 OK elimina personale role admin
 */
async function deleteStaff(req, res) {
    const uuid_staff = req.body
    if (!uuid_staff) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const id = uuid_staff['uuid_staff'];
    idS=validator.blacklist(id, "/'|)");
    const staff = await db.staff.findByPk(idS);
    if (!staff) {
        return res.status(400).json({
            message: "staff non trovato"
        })
    }
    staff.destroy().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}

/**@summary MODIFICA UNO STAFF (ADMIN)
 * @param {req,res} '\editStaff'
 * @param {req} function(req)
 * @param {uuid_staff} string id per identificare uno staff
 * @param {name} string name staff 
 * @param {surname} string cognome staff
 * @param {password} string password staff
 * @param {can_admin} string admin o staff = 1||0
 * @returns {200} res.sendStatus(200) operazione effettuata (modifica staff)
 * @returns {400} res.sendStatus(400) contenuto vuoto || staff non trovato (modifica staff)
 * funzione asincrona di edit Staff : 200 OK modifica staff role admin
 */
async function editStaff(req, res) {
    const { uuid_staff, name, surname, username, password, can_admin } = req.body;
    const staffs = await db.staff.findAll()
    for (const staff of staffs) {
        if (staff.Username == username) {
            return res.status(403).json({
                message: "username gia presente ",
            });
        }
    }
    if (!uuid_staff || !username || !password) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });

    }

    nameS=validator.blacklist(name, "/'|)");
    surnameS=validator.blacklist(surname, "/'|)");
    usernameS=validator.blacklist(username, "/'|)");
    const staff = await db.staff.findByPk(uuid_staff);
    if (!staff) {
        return res.status(400).json({
            message: "staff non trovato",
        });
    }
    staff.Name = nameS
    staff.Surname = surnameS
    staff.Username = usernameS
    staff.Password = password
    staff.CanAdmin = can_admin
    staff.save();

    res.status(200).json({
        message: "operazione effettuata",
    });
}


/**@summary MOSTRA STAFF (ADMIN)
 * @param {req,res} '\showStaff'
 * @param {req} function(req)
 * @param {uuid_staff} string id identifica staff
 * @returns {200} res.sendStatus(200) operazione effettuata (mostra staff)
 * @returns {400} res.sendStatus(400) staff non trovato
 * funzione asincrona di show Staff : 200 OK mostra un  personale staff  role admin
 */
async function showStaff(req, res) {
    const uuid_staff = req.query
    if (!uuid_staff) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const id = uuid_staff['uuid_staff'];
    idS=validator.blacklist(id, "/'|)");
    await db.staff.findOne({
        attributes: ['Name', 'Username', 'Surname', 'Password', 'CanAdmin'],
        where: { 'UuidStaff': idS },
    }).then((staff) => {
        if (!staff) {
            return res.status(400).json({
                message: "staff non trovato",
            });
        }
        res.status(200).json(staff);
    });
}

module.exports = {
    showAllClients,
    deleteClient,
    editClient,
    showClient,
    newClient,
    showReview,
    newStaff,
    editStaff,
    deleteStaff,
    showStaff,
    showAllStaff,
    login,
};