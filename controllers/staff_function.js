const db = require("../models/connection");
const { v4: uuidv4 } = require('uuid');
const jwt = require('../jwt/jwt');
const { getStaffByCredential, showStaffByUsername, createStaff } = require('../service/staff_service');
var validator = require('validator');

/** FUNZIONALITA' STAFF **/

/**@summary REGSITRAZIONE (STAFF)
 * @param {req,res} '\registration'
 * @param {req} function(req)
 * @return {200} res.sendStatus(200) registrato
 * @return {400} res.sendStatus(400) 
 * @param {} data INPUT
 * funzione asincrona di registration: 200 OK registrazione role staff
 */
async function registration(req, res, data) {
    const staff = await createStaff(req, res, data);
    if (staff) {

        const token = jwt.generateTokenStaff(staff);
        console.log(token);
        return res.status(200).json({
            message: 'registrato',
            token: token
        });
    } else {
        res.sendStatus(400);
    }
}

/**@summary LOGIN (STAFF)
 * @param {req,res} '\login'
 * @param {req} function(req)
 * @return {200} res.sendStatus(200) staff autenticato
 * @return {401} res.sendStatus(401) Non autorizzato, credenziali errate
 * @param {} data INPUT
 * funzione asincrona di login: 200 OK Accesso role staff
 */
async function login(req, res, data) {
    const staff = await getStaffByCredential(req, res, data);
    if (!staff) {
        return res.status(401).json({
            message: 'Non autorizzato, credenziali errate',
            code: 401
        });
    } else {

        const token = jwt.generateTokenStaff(staff)
        return res.status(200).json({
            message: 'staff autenticato',
            token: token,
            code: 200
        });

    }
}

/**@summary MOSTRA TAVOLI (STAFF)
 * @param {req,res} '\showTables'
 * @param {req} function(req)
 * @return {200} res.sendStatus(200) file contenente i dati sui tavoli
 * funzione asincrona di show Tables:200 OK Mostra tavoli role staff
 */
async function showTables(req, res) {
    const occupy = await db.occupy.findAll({
        attributes: ['UuidClient'],
        include: {
            model: db.table,
            attributes: ['UuidTable', 'NumberTable', 'Chairs', 'Occupied']
        }
    })
    const table = await db.table.findAll({
        where: { 'Occupied': false },
        attributes: ['UuidTable', 'NumberTable', 'Chairs', 'Occupied']
    });
    const result = { table, occupy }
    res.json(result)
}

/**@summary MODIFICA  PROFILO STAFF (STAFF)
 * @param {req,res} '\edit Staff Profile'
 * @param {req} function(req)
 * @param {username} string username da modificare
 * @return {400} res.sendStatus(400) contenuto vuoto , username gia' presente
 * @return {200} res.sendStatus(200) Operazione effettuata
 * @param {}staff  INPUT
 * funzione asincrona di edit Staff Profile: 200 OK modifica profilo personale role staff
 */
async function editStaffProfile(req, res, staff) {
    const { username } = req.body;
    usernameS=validator.blacklist(username, "/'|)");
    const staffs = await db.staff.findAll()
    if (!username) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }

    for (const staff of staffs) {
        if (staff.Username == usernameS) {
            return res.status(403).json({
                message: "username gia presente ",
            });
        }
    }
    
    const isStaff = await db.staff.findByPk(staff);
    isStaff.Username = usernameS
    isStaff.save().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}

/**@summary MOSTRA PROFILO (STAFF)
 * @param {req,res} '\showProfile'
 * @param {req} function(req)
 * @param {} staff  INPUT
 * @return {} res.json mostra i dati dello staff
 * funzione asincrona di show Profile: 20O OK mostra profilo role staff
 */
async function showProfile(req, res, staff) {
    await db.staff.findOne({
        attributes: ['Name', 'Username', 'Surname'],
        where: { 'UuidStaff': staff },
    }).then((staff) => {
        res.json(staff);
    });
}

/**@summary MOSTRA MAPPA TAVOLI (STAFF)
 * @param {req,res} '\tableMap'
 * @param {req} function(req)
 * @return {400} res.sendStatus(400) contenuto vuoto
 * @return {} res.json dati tavolo
 * funzione asincrona di table Map:200 OK mappa tavoli role staff
 */
async function tableMap(req, res) {
    const uuid_table = req.query;
    if (!uuid_table) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }
    const id = uuid_table['uuid_table'];
    idS=validator.blacklist(id, "/'|)");
    const table = await db.table.findOne({
        where: { 'UuidTable': idS },
        attributes: ['UuidTable', 'NumberTable', 'Chairs', 'Occupied']
    });
    if (!table) {
        return res.status(400).json({
            message: "tavolo non trovato",
        });
    }
    res.json(table)
}

/**@summary MODIFICA TAVOLO (STAFF)
 * @param {req,res} '\editTable'
 * @param {req} function(req)
 * @param {uuid_table} string id del tavolo
 * @param {chairs} snumber sedie per tavolo
 * @param {state} boolean stato libero|| occupato
 * @return {400} res.sendStatus(400) contenuto vuoto
 * @return {200} res.sendStatus(200) operazione effettuata
 * @param {} staff INPUT
 * funzione asincrona di edit Table: 200 OK modifica tavolo role staff
 */
async function editTable(req, res, staff) {
    const { uuid_table, chairs, state } = req.body
    if (!uuid_table || state.isEmpty()) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }
    const table = await db.table.findByPk(uuid_table)
    if (!table) {
        return res.status(400).json({
            message: "tavolo non trovato",
        });
    }
    if (state == false && table.Occupied !== state) {
        await freeTableStatus(table)
    }
    table.Chairs = chairs
    table.save().then((table) => {
        db.organizes.create({
            UuidOrganize: uuidv4(),
            UuidStaff: staff,
            UuidTable: table.UuidTable
        })
    }).then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}

/**@summary  FREE TABLE STATUS(STAFF)
 * @param {req,res} '\freeTable'
 * @param {req} function(req)
 * funzione asincrona di free Table:  tavoli liberi role staff
 */
async function freeTableStatus(table) {
    table.Occupied = false;
    const occupy = await db.occupy.findOne({
        where: { 'UuidTable': table.UuidTable }
    })
    await occupy.destroy()
    await table.save()
}


//FREETABLES
async function freeTable(req, res) {
    const { uuid_table } = req.query;
    if (!uuid_table) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }
    const table = await db.table.findByPk(uuid_table);
    if (!table) {
        return res.status(400).json({
            message: "il tavolo non esiste",
        });
    }
    if (table.Occupied == false) {
        return res.status(400).json({
            message: "il tavolo è gia libero"
        })
    }
    table.Occupied = false;
    await table.save().then(() => res.sendStatus(200));
}

/**@summary  CATEGORIA PIATTO (STAFF)
 * @param {req,res} '\categoryDish'
 * @param {req} function(req)
 * @param {uuid_dish} string id del piatto
 * @return {400} res.sendStatus(400) non trovato
 * @return {200} res.sendStatus(200) dati piatti per categoria
 * funzione asincrona di category Dish: 200 OK categoria piatto role staff
 */
async function categoryDish(req, res) {
    const { uuid_dish } = req.query;
    if (!uuid_dish) {
        return res.sendStatus(400);
    }
    idS=validator.blacklist(uuid_dish, "/'|)");
    db.belong.findAll({
        where: { 'UuidDish': idS },
        attributes: ['UuidCategory'],
        include: {
            model: db.category,
            attributes: ['CategoryName']
        }
    }).then((dish) => res.status(200).json(dish));
}

/**@summary  CREA TAVOLO (STAFF)
 * @param {req,res} '\createTable'
 * @param {req} function(req)
 * @param {} staff  INPUT
 * @return {200} res.sendStatus(200) operazione effettuata
 * funzione asincrona di createTable: 200 OK crea tavolo
 */
async function createTable(req, res, staff) {

    await db.table.create({
        UuidTable: uuidv4(),
        Occupied: 0,
        Chairs: 4,

    }).then(() => res.status(200).json({ message: "operazione effettuata" }));
}

module.exports = {
    registration,
    login,
    editStaffProfile,
    showTables,
    showProfile,
    tableMap,
    editTable,
    freeTable,
    categoryDish,
    createTable

}