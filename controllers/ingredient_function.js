const db = require("../models/connection");
const { v4: uuidv4 } = require('uuid');

/** FUNZIONALITA' INGREDIENTI **/

/**@summary CREA INGREDIENTE(ADMIN)
 * @param {req,res} '\newIngredient'
 * @param {req} function(req)
 * @param {name} string nome ingrediente
 * @return {200} res.sendStatus(200) operazione effettuata con successo (creazione ingrediente)
 * @return {400} res.sendStatus(400) il contenuto non puo essere vuoto 
 * funzione asincrona di new Ingredient:200 OK crea nuovo ingrediente role admin
 */
async function newIngredient(req, res) {
    const { name } = req.body;
    if (!name) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }

    const ingredientToFind = await db.ingredient.findAll({
        where: { Name: name }
    });

    if (ingredientToFind.length > 0) {
        console.log("wait")
        return res.status(403).json({

            message: "ingrediente gia' presente",
        });
    }

    db.ingredient.create({
        UuidIngredient: uuidv4(),
        Name: name
    }).then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}


/**@summary ELIMINA INGREDIENTE(ADMIN)
 * @param {req,res} '\deleteIngredient'
 * @param {req} function(req)
 * @param {uuid_ingredient} string id ingrediente da eliminare
 * @return {200} res.sendStatus(200) opereazione effettuata (eliminazione ingrediente)
 * @return {400} res.sendStatus(400) contenuto non puo' essere vuoto
 * funzione asincrona di delete Ingredient: 200 OK  elimina ingrediente role admin
 */
async function deleteIngredient(req, res) {
    const { uuid_ingredient } = req.body;
    if (!uuid_ingredient) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }
    const ingredient = await db.ingredient.findByPk(uuid_ingredient);
    ingredient.destroy().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}


/**@summary RINOMINA INGREDIENTE (ADMIN)
 * @param {req,res} '\renameIngredient'
 * @param {req} function(req)
 * @param {uuid_ingredient} string id ingrediente da rinominare
 * @return {200} res.sendStatus(200) operazione effettuata (rinomina ingrediente)
 * @return {200} res.sendStatus(400) contenuto non puo' essere vuoto
 * @return {404} res.sendStatus(404) ingrediente gia' presente
 * funzione asincrona di rename Ingredient: rinomina ingrediente role admin
 */
async function renameIngredient(req, res) {
    const { uuid_ingredient, name } = req.body;
    if (!uuid_ingredient || !name) {
        return res.status(400).json({

            message: "il contenuto non puo essere vuoto",
        });
    }

    const ingredientToFind = await db.ingredient.findAll({
        where: { Name: name }
    });

    if (ingredientToFind.length > 0) {
        return res.status(404).json({

            message: "ingrediente gia' presente",
        });
    }
    const ingredient = await db.ingredient.findByPk(uuid_ingredient);
    ingredient.Name = name;
    ingredient.save().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}

/**@summary MOSTRA INGREDIENTI (ADMIN)
 * @param {req,res} '\getAllIngredients'
 * @param {req} function(req)
 * @return {200} res.sendStatus(200) file json contente tutti gli ingredientu (mostra ingredienti)
 * funzione asincrona di get All Ingredients : 200 OK ottieni tutti gli ingredienti
 */
async function getAllIngredients(req, res) {
    const ingredient = await db.ingredient.findAll({
        attributes: ['Name', 'UuidIngredient']
    }).then((ingredients) => res.status(200).json(ingredients));
}

module.exports = {
    getAllIngredients,
    newIngredient,
    renameIngredient,
    deleteIngredient
}