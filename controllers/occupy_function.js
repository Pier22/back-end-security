const db = require('../models/connection');
const { v4: uuidv4 } = require('uuid');

/** FUNZIONALITA' OCCUPY **/

/**@summary OCCUPA TAVOLO (ADMIN)
 * @param {req,res} '\occupyTable'
 * @param {req} function(req)
 * @param {uuid_table} string id tavolo
 * @return {200} res.sendStatus(200) operazione effettuata (prenota tavolo)
 * @return {400} res.sendStatus(400) contenuto vuoto
 * @return {404} res.sendStatus(404) tavolo non presente
 * @return {403} res.sendStatus(403) tavolo gia' occupato
 * @param {} client INPUT
 * funzione asincrona di occupy Table: 200 OK occupa Tavolo
 */
async function occupyTable(req, res, client) {
    const { uuid_table } = req.body;
    if (!uuid_table) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const table = await db.table.findByPk(uuid_table)

    if (!table) {
        return res.status(404).json({
            message: "Tavolo non trovato",
        });
    }
    if (table.Occupied == true) {
        return res.status(403).json({
            message: "Tavolo gia occupato",
        });
    }
    await db.occupy.findOne({
        where: { UuidClient: client, Date: Date.now() },
        attributes: ['UuidClient']
    }).then((resu) => {
        if (resu) {
            return res.status(403).json({
                message: "Hai già effettuato una prenotazione",
            });
        } else {
            return db.occupy.create({
                UuidOccupy: uuidv4(),
                UuidClient: client,
                UuidTable: uuid_table,
                Date: Date.now(),
                Hour: getTime()
            }).then(() => {
                table.Occupied = true;
                table.save().then(res.status(200).json({
                    message: "operazione effettuata con successo",
                    code: 200
                }));
            })
        }
    });
}


/**@summary RIMUOVI OCCUPAZIONE TAVOLO (CLIENTE)
 * @param {req,res} '\deleteOccupation'
 * @param {req} function(req)
 * @param {uuid_occupy} string id occupazione da eliminare
 * @return {200} res.sendStatus(200) operazione effettuata (rimuovi prenotazione tavolo)
 * @return {400} res.sendStatus(400) contenuto vuoto
 * @param {} client INPUT
 * funzione asincrona di delete Occupation:200 OK elimina occupazione
 */
async function deleteOccupation(req, res) {
    date = new Date();
    const { uuid_occupy } = req.body;
    if (!uuid_occupy) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const occupy = await db.occupy.findByPk(uuid_occupy)
    if (!occupy) {
        return res.status(400).json({
            message: "Prenotazione non trovata",
        });
    } else if (occupy.Date !== date.toISOString().split("T")[0]) {
        return res.status(400).json({
            message: "Non puoi eliminare questa prenotazione",
        });
    }
    const table = await db.table.findByPk(occupy.UuidTable);
    table.Occupied = false
    table.save();
    occupy.destroy().then(() => {
        res.status(200).json({
            message: "operazione effettuata",
            code: 200
        })
    });
}


/**@summary TAVOLI PRENOTATI (CLIENTE)
 * @param {req,res} '\showMyTables'
 * @param {req} function(req)
 * @return {200} res.sendStatus(200) file json contente i tavoli prenotati
 * @param {} client INPUT
 * funzione asincrona di show My Tables: 200 OK mostra i miei tavoli
 */
function showMyTables(req, res, client) {
    db.occupy.findAll({
        attributes: ['Date', 'Hour', 'UuidOccupy'],
        where: { UuidClient: client },
        include: {
            model: db.table,
            attributes: ['NumberTable', 'Chairs']
        }
    }).then(table => res.status(200).json(table));
}


/**@summary MOSTRA TAVOLI LIBERI 
 * @param {req,res} '\showFreeTables'
 * @param {req} function(req)
 * @return {200} res.sendStatus(200) file json cointente i tavoli liberi
 * funzione di show Free Tables: 200 OK mostra tavoli liberi
 */
function showFreeTables(req, res) {
    db.table.findAll({
        where: { Occupied: false },
    }).then(freeTable => res.status(200).json(freeTable));
}


/**@summary CONTROLLA TAVOLI (TAVOLI LIBERATI DAL SISTEMA)
 * @param {req,res} '\tableControl'
 * @param {req} function(req)
 * @param {} next NEXT CALL
 * funzione di table Control: controlla tavoli
 */
function tableControl(req, res, next) {
    setTimeout(switchOccupy, 5000)
}


/**@summary LIBERA TAVOLO (STAFF)
 * @param {} '\switchOccupy'
 * funzione di switch Occupy: controlla tavoli
 */
function switchOccupy() {
    db.table.update({
        Occupied: false
    }, {
        where: { Occupied: true }
    })
}


/** GET TIME
 * @param {} '\getTime'
 * funzione di get Time: ritorna tempo
 */
function getTime() {
    const d = Date(Date.now());
    const time = d.split(" ")
    return time[4];
}

module.exports = {
    occupyTable,
    showMyTables,
    deleteOccupation,
    showFreeTables,
    tableControl
};