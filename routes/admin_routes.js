const express = require('express');
const router = express.Router();
const { isAdmin } = require('../middleware/aut_jwt');
const { deleteReview } = require('../controllers/review_functions');
const { showAllClients, deleteClient, editClient, showClient, newClient, showReview, newStaff, editStaff, deleteStaff, showStaff, showAllStaff } = require('../controllers/admin_function')
const { createDish, showAllDishes, editDish, deleteDish } = require('../controllers/dish_function');
const { login} = require('../controllers/admin_function');
const { getAllIngredients, newIngredient, deleteIngredient, renameIngredient } = require('../controllers/ingredient_function');
const { getAllAllergens, newAllergen, deleteAllergen, renameAllergen } = require('../controllers/allergen_function')
const { validationResult, body } = require('express-validator');
const { newRole } = require('../controllers/role_function');
const limit_call = require('../middleware/Limiter')
const config = require('../config')


/** MODULO ADMIN ROUTES -> RACCHIUDE TUTTE LE ROTTE ALLE VARIE FUNZIONI INVOCABILI DALL'ENTITA' ADMIN**/
// API AMIN DOC : https://documenter.getpostman.com/view/11778714/Szzn4vAE?version=latest#be5f9427-62e1-4fa9-bc50-ca91b6d3949b

/** body params : username,password
 * @param {username} string username admin
 * @param {password} string passwordadmin
 * @param  {req} '/login' -> accesso admin
 * @login -> funzione interiore
 */
/**
 * @swagger
 * /login admin:
 *   post:
 *      description: login admin username,password
 *      responses:
 *      '200':
 *        description: Successfully created user
 *   parameters:
 *      - name: postman API
 *        in: query
 *        description: use postman API to run it
 *        required: false
 */

router.post('/login',limit_call.apiLimiter, [
    body('username').trim().notEmpty().withMessage('username is not valid'),
    body('password').trim().notEmpty().withMessage('Password < 5 Caratteri')

], (req, res) =>  {
   
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    } else {
        const { username, password } = req.body;
        config.logger.info("ADMIN log-in")
        login(req, res, { username, password });
    }
});

/**
 * @param {req,res} '\delete_dish'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di delete dish: elimina il piatto role admin
 */
/**
 * @swagger
 * /deleted_dish:
 *  post:
 *    description: elimina piatto
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/delete_dish', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("Delete-dish - Admin",admin)
        deleteDish(req, res)
    }

});

/**
 * @param {req,res} '\new_dish'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di new dish: nuovo il piatto role admin
 */
/**
 * @swagger
 * /new_dish:
 *  post:
 *    description: crea piatto
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.post('/new_dish', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("new-dish - Admin",admin)
        createDish(req, res, admin)
    }

});

/**
 * @param {req,res} '\show_dishes'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di show dishes: mostra piatti role admin
 */
/**
 * @swagger
 * /show_dishes:
 *  get:
 *    description: mostra piatti
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.get('/show_dishes', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("show_dishes - Admin",admin)
        showAllDishes(req, res)
    }
});

/**
 * @param {req,res} '\edit_dish'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di edit dish: modifica piatto role admin
 */
/**
 * @swagger
 * /edit_dishes:
 *  post:
 *    description: modifica piatto
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.post('/edit_dish', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("edit_dish - Admin",admin)
        editDish(req, res, admin)
    }

});


/**
 * @param {req,res} '\delete_review'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di delete review: elimina recensione role admin
 */
/**
 * @swagger
 * /delete_review:
 *  post:
 *    description: elimina recensione
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.post('/delete_review', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("delete_review - Admin",admin)
        deleteReview(req, res, admin)
    }

});


/**
 * @param {req,res} '\show_clients'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di show clients: mostrare ai clienti role admin
 */
/**
 * @swagger
 * /show_clients:
 *  get:
 *    description: mostra clienti
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.get('/show_clients', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("show_clients - Admin",admin)
        showAllClients(req, res)
    }

});


/**
 * @param {req,res} '\delete_clients'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di delete client: elimina cliente role admin
 */
/**
 * @swagger
 * /delete_clients:
 *  post:
 *    description: elimina cliente
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.post('/delete_client', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("delete_client - Admin",admin)
        deleteClient(req, res)
    }
});


/**
 * @param {req,res} '\edit_client'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di edit client: modifica cliente role admin
 */
/**
 * @swagger
 * /edit_clients:
 *  post:
 *    description: modifica cliente
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.post('/edit_client', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("edit_client - Admin",admin)
        editClient(req, res)
    }

});


/**
 * @param {req,res} '\show_client'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di show client: mostra cliente role admin
 */
/**
 * @swagger
 * /show_clients:
 *  get:
 *    description: mostra cliente
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.get('/show_client', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("show_client - Admin",admin)
        showClient(req, res)
    }

});


/**
 * @param {req,res} '\new_client'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di new client: nuovo cliente role admin
 */
/**
 * @swagger
 * /new_client:
 *  post:
 *    description: crea cliente
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.post('/new_client', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("new_client - Admin",admin)
        newClient(req, res)
    }
});


/**
 * @param {req,res} '\new_staff'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di new staff: nuovo personale role admin
 */
/**
 * @swagger
 * /new_staff:
 *  post:
 *    description: crea membro staff
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.post('/new_staff', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("new_staff - Admin",admin)
        newStaff(req, res)
    }

});

/**
 * @param {req,res} '\show_all_staff'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di show all staff: mostra tutto lo personale role admin
 */
/**
 * @swagger
 * /show_all_staff:
 *  get:
 *    description: mostra tutti i membri dello staff
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.get('/show_all_staff', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("show_all_staff - Admin",admin)
        showAllStaff(req, res)
    }

});

/**
 * @param {req,res} '\delete_staff'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di delete staff: elimina personale role admin
 */
/**
 * @swagger
 * /delete_staff:
 *  post:
 *    description: elimina un membro dello staff
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.post('/delete_staff', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("delete_staff - Admin",admin)
        deleteStaff(req, res)
    }

});

/**
 * @param {req,res} '\edit_staff'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di edit staff: modifica personale role admin
 */
/**
 * @swagger
 * /edit_staff:
 *  post:
 *    description: modifica staff
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.post('/edit_staff', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("edit_staff - Admin",admin)
        editStaff(req, res)
    }

});


/**
 * @param {req,res} '\show_staff'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di show staff: mostra un personale staff role admin
 */
/**
 * @swagger
 * /show_staff:
 *  get:
 *    description: mostra un singolo memebro staff
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.get('/show_staff', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("show_staff - Admin",admin)
        showStaff(req, res)
    }

});


/**
 * @param {req,res} '\show_all_review'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di show all review: mostra tutte le recensioni role admin
 */
/**
 * @swagger
 * /show_all_review:
 *  get:
 *    description: mostra tutte le review
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.get('/show_all_review', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("show_all_review - Admin",admin)
        showReview(req, res)
    }
});

//TO TEST
router.get('/show_all_review', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("test:",admin)
        showReview(req, res)
    }
});



/**
 * @param {req,res} '\get_ingredient'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di get ingredient: ottenere l'ingrediente role admin
 */
/**
 * @swagger
 * /get_ingredient:
 *  get:
 *    description: mostra gli ingredienti
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.get('/get_ingredient', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("get_ingredient - Admin",admin)
        getAllIngredients(req, res)
    }
});



/**
 * @param {req,res} '\get_allergens'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di get allergens: ottenere allergene role admin
 */
/**
 * @swagger
 * /get_allergen:
 *  get:
 *    description: mostra gli allergeni
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.get('/get_allergens', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("get_allergens - Admin",admin)
        getAllAllergens(req, res)
    }
});

/**
 * @param {req,res} '\new_ingredient'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di new ingredient: nuovo ingrediente role admin
 */
/**
 * @swagger
 * /new_ingredient:
 *  post:
 *    description: crea un nuovo ingrediente
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.post('/new_ingredient', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("new_ingredient - Admin",admin)
        newIngredient(req, res)
    }
});

/**
 * @param {req,res} '\new_allergen'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di new allergen: nuovo allergene role admin
 */
/**
 * @swagger
 * /new_allergen:
 *  post:
 *    description: crea un nuovo allergene
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.post('/new_allergen', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("new_allergen - Admin",admin)
        newAllergen(req, res)
    }
});

/**
 * @param {req,res} '\delete_ingredient'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di delete ingredient: elimina ingrediente role admin
 */
/**
 * @swagger
 * /delete_ingredient:
 *  post:
 *    description: elimina un ingrediente
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.post('/delete_ingredient', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("delete_ingredient- Admin",admin)
        deleteIngredient(req, res)
    }
});

/**
 * @param {req,res} '\delete_allergen'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di delete allergen: elimina allergene role admin
 */
/**
 * @swagger
 * /delete_allergen:
 *  post:
 *    description: elimina un allergene
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.post('/delete_allergen', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("delete_allergen- Admin",admin)
        deleteAllergen(req, res)
    }
});

/**
 * @param {req,res} '\rename_ingredient'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di rename ingredient: rinominare l'ingrediente role admin
 */
/**
 * @swagger
 * /get_ingredient:
 * post:
 *  rename_ingredient:
 *    description: rinomina un ingrediente
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.post('/rename_ingredient', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("rename_ingredient - Admin",admin)
        renameIngredient(req, res)
    }
});

/**
 * @param {req,res} '\rename_allergen'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di rename allergen: rinominare allergene role admin
 */
/**
 * @swagger
 * /rename_allergen:
 *  post:
 *    description: rinomina un allergene
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.post('/rename_allergen', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("rename_allergen - Admin",admin)
        renameAllergen(req, res)
    }
});


/**
 * @param {req,res} '\new_role'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione new_role: crea un nuovo ruolo
 */
/**
 * @swagger
 * /new_role:
 *  post:
 *    description: crea un nuovo ruolo
 *    responses:

 *      '200':
 *        description: A successful response
 */
router.post('/new_role', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        config.logger.info("new_role - Admin",admin)
        newRole(req, res)
    }
});




module.exports = router;