const express = require('express');
const router = express.Router();
const { isClient } = require('../middleware/aut_jwt');
const { registration, login, showAccount, editAccount, getAllDishesByCategory,showReviewByTitle,showReview, Login,test,test2,LoginTOKEN } = require('../controllers/client_functions');
const { reservationDish, showAllReservations, editReservation, validateReservation, deleteDishesfromReservation, showMyReservations, deleteReservation } = require('../controllers/reservation_function');
const { occupyTable, showMyTables, deleteOccupation, showFreeTables } = require('../controllers/occupy_function');
const { writeReview, showMyReviews, deleteReview } = require('../controllers/review_functions');
const { validationResult, body } = require('express-validator');
var client;
const { createDish, showAllDishes, editDish, dishesByCategory, showDetailDish, deleteDish } = require('../controllers/dish_function')
const limit_call= require('../middleware/Limiter')
const config = require('../config')

/** MODULO CLIENT ROUTES -> RACCHIUDE TUTTE LE ROTTE ALLE VARIE FUNZIONI INVOCABILI DALL'ENTITA' CLIENT**/
// API CLIENT DOC: https://documenter.getpostman.com/view/11778714/Szzn4vAE?version=latest#9463f0ba-05d4-49ac-b4b9-a1c6bf498a4b

/** body params : password,email
 * @param  {req} '/login' -> accesso client
 * @param {email} string email client
 * @param {password} string password
 * @login -> funzione interiore
 */
/**
 * @swagger
 * /login client:
 *   post:
 *      description: login client email,password
 *      responses:
 *      '200':
 *        description: Successfully created user
 *   parameters:
 *      - name: postman API
 *        in: query
 *        description: use postaman API to run it
 *        required: false
 */
router.post('/login', limit_call.apiLimiter, [
    // controllo dati prima di prcedere al login
    body('password').trim().notEmpty().withMessage('Password < 5 Caratteri'),
    body('email').trim().notEmpty().isEmail().withMessage('email is not valid')

], (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
            errors: errors.array(),
        });
    } else {
        const { email, password } = req.body;
        config.logger.debug("User log-in")
        login(req, res, { email, password });
    }
});


/** body params : password,email,role,username
 * @param  {req} 
 * @param {email} string email client
 * @param {username} string username client
 * @param {role} string uuid role client
 * @param {password} string password
 * @registration -> funzione interna
 */
/**
 * @swagger
 * /signup:
 *  post:
 *    description: registarti
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/signup', [

    // controllo dati prima di prcedere alla registrazione
    body('password').trim().notEmpty().isLength({ min: 5 }).withMessage('Password < 5 Caratteri'),
    body('email').trim().notEmpty().isEmail().withMessage('email is not valid'),
    body('role').trim().notEmpty().withMessage('role is not valid'),
    body('username').trim().notEmpty().withMessage('username is not valid'),


], (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
            errors: errors.array(),
        });
    } else {

        const { email, username, password, role, confirmpassword } = req.body;

        if (password !== confirmpassword) {
            return res.status(400).json({
                message: 'Campi password e confirm password diversi',
            });
        } else {
            registration(req, res, { email, username, password, role });
        }
    }
});


/**
 * @param {req,res} '\show_account'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di show account: mostra account role client
 */
/**
 * @swagger
 * /show account:
 *  get:
 *    description: mostra dati account
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/show_account', function(req, res) {
    client = isClient(req, res);
    if (client) {
        config.logger.info("show_account - client ",client)
        showAccount(req, res, client)
    }
});


/**
 * @param {req,res} '\edit_account'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di edit account: modifica account role client
 */
/**
 * @swagger
 * /edit account:
 *  post:
 *    description: modifica account
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/edit_account', function(req, res) {
    client = isClient(req, res);
    if (client) {
        config.logger.info("edit_account - client  ", client)
        editAccount(req, res, client)
    }
});


/**
 * @param {req,res} '\dish_reservation'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di dish reservation: prenota un piatto role client
 */
/**
 * @swagger
 * /reservation dish:
 *  post:
 *    description: prenota piatto
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/dish_reservation', validateReservation, function(req, res) {
    client = isClient(req, res);
    if (client) {
        config.logger.info("dish_reservation - client", client)
        reservationDish(req, res, client)
    }
});


/**
 * @param {req,res} '\show_all_reservation'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di show all reservation: mostra tutte le prenotazioni role client
 */
/**
 * @swagger
 * /show all reservation:
 *  get:
 *    description: mostra prenotazioni
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/show_all_reservation', function(req, res) {

    client = isClient(req, res);
    if (client) {
        config.logger.info("show_all_reservation - client", client)
        showAllReservations(req, res, client)
    }

});

/**
 * @param {req,res} '\occupy_table'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di occupy table: occupa tavolo role client
 */
/**
 * @swagger
 * /occupy table:
 *  post:
 *    description: tavoli occupati
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/occupy_table', validateReservation, function(req, res) {
    client = isClient(req, res);
    if (client) {
        config.logger.info("occupy_table - client", client)
        occupyTable(req, res, client)
    }
});


/**
 * @param {req,res} '\show_tables'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di show tables: mostra i tavoli role client
 */
/**
 * @swagger
 * /show_tables:
 *  get:
 *    description: mostra tavoli
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/show_tables', function(req, res) {
    client = isClient(req, res);
    if (client) {
        config.logger.info("show_tables - client", client)
        showMyTables(req, res, client)
    }
});


/**
 * @param {req,res} '\show_free_tables'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di show free tables: mostra tavoli liberi role client
 */
/**
 * @swagger
 * /show_free_tables:
 *  get:
 *    description: mostra tavoli liberi
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/show_free_tables', function(req, res) {
    client = isClient(req, res);
    if (client) {
        config.logger.info("show_free_tables - client", client)
        showFreeTables(req, res, client)
    }
});

/**
 * @param {req,res} '\detail_dish'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di detail dish: dettaglio piatto role client
 */
/**
 * @swagger
 * /detail dish:
 *  get:
 *    description: dettaglio piatto
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/detail_dish', function(req, res) {
    client = isClient(req, res);
    if (client) {
        config.logger.info("detail_dish - client", client)
        showDetailDish(req, res)
    }
});

/**
 * @param {req,res} '\category_dish'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di category dishs: categoria piatto role client
 */
/**
 * @swagger
 * /category dish:
 *  get:
 *    description: categoria piatto
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/category_dish', function(req, res) {
    client = isClient(req, res);
    if (client) {
        config.logger.info("category_dish - client", client)
        dishesByCategory(req, res)
    }
});

/**
 * @param {req,res} '\delete_occupation'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di delete occupation: elimina posto tavolo occupato role client
 */
/**
 * @swagger
 * /delete_occupation:
 *  post:
 *    description: mostra tavoli
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/delete_occupation', validateReservation, function(req, res) {
    client = isClient(req, res);
    if (client) {
        config.logger.info("delete_occupation - client", client)
        deleteOccupation(req, res, client)
    }
});

/**
 * @param {req,res} '\edit_reservation'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di edit reservation: modifica tavolo occupato role client
 */
/**
 * @swagger
 * /edit_reservation:
 *  post:
 *    description: modifica prenotazione
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/edit_reservation', validateReservation, function(req, res) {
    client = isClient(req, res);
    if (client) {
        config.logger.info("edit_reservation - client", client)
        editReservation(req, res, client)
    }
});

/**
 * @param {req,res} '\delete_dishes_reservation'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di delete dishes reservation: cancella prenotazione piatti role client
 */
/**
 * @swagger
 * /delete_dishes_reservation:
 *  post:
 *    description: elimina prenotazione piatto
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/delete_dishes_reservation', validateReservation, function(req, res) {
    client = isClient(req, res);
    if (client) {
        config.logger.info("delete_dishes_reservation - client", client)
        deleteDishesfromReservation(req, res, client)
    }
});

/**
 * @param {req,res} '\my_reservations'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di my reservations: la mia prenotazione role client
 */
/**
 * @swagger
 * /my_reservation:
 *  get:
 *    description: mostra le mie prenotazioni
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/my_reservations', function(req, res) {
    client = isClient(req, res);
    if (client) {
        config.logger.info("my_reservations - client", client)
        showMyReservations(req, res, client)
    }
});

/**
 * @param {req,res} '\write_review'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di write review: scrivere una recensione role client
 */
/**
 * @swagger
 * /write_review:
 *  post:
 *    description: scrivi una recensione
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/write_review', function(req, res) {
    client = isClient(req, res);
    if (client) {
        config.logger.info("write_review - client", client)
        writeReview(req, res, client)
    }
});

/**
 * @param {req,res} '\show_reviews'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di show reviews: mostra le recensioni role client
 */
/**
 * @swagger
 * /show_review:
 *  get:
 *    description: mostra recensioni
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/show_reviews', function(req, res) {
    client = isClient(req, res);
    if (client) {
        config.logger.info("show_reviews - client", client)
        showMyReviews(req, res, client)
    }
});

/**
 * @param {req,res} '\delete_review'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di delete review: elimina una recensione role client
 */
/**
 * @swagger
 * /delete_review:
 *  post:
 *    description: rimuovi una recensione
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/delete_review', function(req, res) {
    client = isClient(req, res);
    if (client) {
        config.logger.info("delete_review - client", client)
        deleteReview(req, res, client)
    }
});

/**
 * @param {req,res} '\all_dishes'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di all dishes: tutti i piatti role client
 */
/**
 * @swagger
 * /all_dishes:
 *  get:
 *    description: mostra piatti
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/all_dishes', function(req, res) {
    client = isClient(req, res);
    if (client) {
        config.logger.info("all_dishes - client", client)
        getAllDishesByCategory(req, res)
    }
});

/**
 * @param {req,res} '\delete_all_reservation'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di delete all reservation: elimina tutta la prenotazione role client
 */
/**
 * @swagger
 * /delete_all_reservation:
 *  post:
 *    description: rimuovi tutte le prenotazioni
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/delete_all_reservation', function(req, res) {
    client = isClient(req, res);
    if (client) {
        config.logger.info("delete_all_reservation - client", client)
        deleteReservation(req, res, client)
    }
});




module.exports = router;