const express = require('express');
const router = express.Router();
const { isStaff, isAdminStaff } = require('../middleware/aut_jwt');
const { editStaffProfile, showTables, showProfile, tableMap, editTable, registration, login, freeTable, categoryDish, createTable } = require('../controllers/staff_function')
const { createDish, showAllDishes, editDish, dishesByCategory, showDetailDish, deleteDish, showDish, getDish, getAllDishes } = require('../controllers/dish_function')
const { validationResult, body } = require('express-validator');
const { getAllCategories, newCategory } = require('../controllers/category_function');
const { getAllIngredients } = require('../controllers/ingredient_function');
const { getAllAllergens } = require('../controllers/allergen_function');
const { getAllRole } = require('../controllers/role_function')
var staff;
const limit_call= require('../middleware/Limiter')
const config = require('../config')

/** MODULO STAFF ROUTES -> RACCHIUDE TUTTE LE ROTTE ALLE VARIE FUNZIONI INVOCABILI DALL'ENTITA' STAFF**/
//API STAFF DOC: https://documenter.getpostman.com/view/11778714/Szzn4vAE?version=latest#a59dad2e-afe1-4368-865c-84ec7e883d13

/** body params : password,username
 * @param  {req} '/signup' -> registrazione staff
 * @param  {username} string username staf
 * @param {passowrd} string password staf
 * @registration -> funzione interiore
 */
/**
 * @swagger
 * /signup:
 *  post:
 *    description: registrati come staff
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/signup', [

    // controllo dati prima di prcedere alla registrazione
    body('username').trim().notEmpty().withMessage('username is not valid'),
    body('password').trim().notEmpty().isLength({ min: 5 }).withMessage('Password < 5 Caratteri')

], (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    } else {

        const { username, password, confirmpassword } = req.body;

        if (password !== confirmpassword) {
            console.log("passworde sbagliata");
            return res.sendStatus(400);
        } else {
            registration(req, res, { username, password });
        }
    }
});

/** body params : username,password
 * @param  {req} '/signup' -> registrazione staff
 * @param  {username} string username staf
 * @param {passowrd} string password staf
 * @login -> funzione interiore
 */
/**
 * @swagger
 * /login:
 *  post:
 *    description: accedi come staff
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/login', limit_call.apiLimiter,[

    body('username').trim().notEmpty().withMessage('username is not valid'),
    body('password').trim().notEmpty().withMessage('Password < 5 Caratteri')

], (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    } else {
        const { username, password } = req.body;
        config.logger.info("login Staff")
        login(req, res, { username, password });

    }
});


/**
 * @param {req,res} '\category_dish'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di category_dish: catalogo piatto role staff
 */
/**
 * @swagger
 * /category dish:
 *  get:
 *    description: categoria piatto
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/category_dish', function(req, res) {
    staff = isStaff(req, res)
    if (staff) {
        config.logger.info("category_dish - Staff",staff)
        dishesByCategory(req, res)
    }
});

/**
 * @param {req,res} '\delete_dish'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di delete dish: elimina il piatto role staff
 */
/**
 * @swagger
 * /delete_dish:
 *  post:
 *    description: rimuovi piatto
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/delete_dish', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        config.logger.info("delete_dish - Staff",staff)
        deleteDish(req, res)
    }
});

/**
 * @param {req,res} '\detail_dish'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di detail dish: piatto di dettaglio role staff
 */
/**
 * @swagger
 * /detail dish:
 *  get:
 *    description: dettaglio piatto
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/detail_dish', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        config.logger.info("detail_dish - Staff",staff)
        showDetailDish(req, res)
    }
});

/**
 * @param {req,res} '\new_dish'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di new dish: nuovo piatto role staff
 */
/**
 * @swagger
 * /new dish:
 *  post:
 *    description: crea nuovo piatto
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/new_dish', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        config.logger.info("new_dish - Staff",staff)
        createDish(req, res, staff)
    }

});

/**
 * @param {req,res} '\show_dishes'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di show dishes: mostra i piatti role staff
 */
/**
 * @swagger
 * /show_review:
 *  get:
 *    description: mostra piatto
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/show_dishes', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        config.logger.info("show_dishes - Staff",staff)
        showAllDishes(req, res)
    }

});


/**
 * @param {req,res} '\edit_dish'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di edit dish: modifica piatto role staff
 */
/**
 * @swagger
 * /edit dish:
 *  post:
 *    description: mostra piatto
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/edit_dish', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        config.logger.info("edit_dish - Staff",staff)
        editDish(req, res, staff)
    }

});

/**
 * @param {req,res} '\show_tables'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di show tables: mostra tavolo role staff
 */
/**
 * @swagger
 * /show_tables:
 *  get:
 *    description: mostra tavoli
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/show_tables', function(req, res) {
    staff = isStaff(req, res)
    if (staff) {
        config.logger.info("show_tables - Staff",staff)
        showTables(req, res)
    }

});

/**
 * @param {req,res} '\show_profile'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di show profile: mostra profilo role staff
 */
/**
 * @swagger
 * /show_profile:
 *  get:
 *    description: mostra profilo
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/show_profile', function(req, res) {
    staff = isStaff(req, res)
    if (staff) {
        config.logger.info("show_profile - Staff",staff)
        showProfile(req, res, staff)
    }
});

/**
 * @param {req,res} '\edit_profile'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di edit profile: modifica profilo role staff
 */
/**
 * @swagger
 * /edit_profile:
 *  post:
 *    description: modifica profilo
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/edit_profile', function(req, res) {
    staff = isStaff(req, res)
    if (staff) {
        config.logger.info("show_profile - Staff",staff)
        editStaffProfile(req, res, staff)
    }
});


/**
 * @param {req,res} '\table_map'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di table map: mappa tavoli role staff
 */
/**
 * @swagger
 * /table map:
 *  get:
 *    description: mappa tavoli
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/table_map', function(req, res) {
    staff = isStaff(req, res)
    if (staff) {
        config.logger.info("table_map - Staff",staff)
        tableMap(req, res)
    }
});

/**
 * @param {req,res} '\edit_table'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di edit table: modifica tavoli role staff
 */
/**
 * @swagger
 * /edit table:
 *  post:
 *    description: modifica tavolo
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/edit_table', function(req, res) {
    staff = isStaff(req, res)
    if (staff) {
        config.logger.info("edit_table - Staff",staff)
        editTable(req, res, staff)
    }
});

/**
 * @param {req,res} '\free_table'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di free table: tavoli liberi role staff
 */
/**
 * @swagger
 * /free table:
 *  get:
 *    description: mostra tavoli liberi
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/free_table', function(req, res) {
    staff = isStaff(req, res)
    if (staff) {
        config.logger.info("free_table - Staff",staff)
        freeTable(req, res, staff)
    }
});

/**
 * @param {req,res} '\get_categories'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di get categories: ottenere categorie role staff
 */
/**
 * @swagger
 * /get category:
 *  get:
 *    description: ottieni gategorie
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/get_categories', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        config.logger.info("get_categories - Staff",staff)
        getAllCategories(req, res)
    }
});


/**
 * @param {req,res} '\get_ingredients'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di get ingredients: prendi gli ingredienti role staff
 */
/**
 * @swagger
 * /get ingredient:
 *  get:
 *    description: ottieni ingredienti
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/get_ingredients', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        config.logger.info("get_ingredients - Staff",staff)
        getAllIngredients(req, res)
    }
});

/**
 * @param {req,res} '\get_allergens'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di get allergens: ottenere allergeni role staff
 */
/**
 * @swagger
 * /get allergen:
 *  get:
 *    description: ottieni allergeni
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/get_allergens', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        config.logger.info("get_allergens - Staff",staff)
        getAllAllergens(req, res)
    }
});

/**
 * @param {req,res} '\get_category'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di get category: Ottieni categoria role staff
 */
/**
 * @swagger
 * /get category:
 *  get:
 *    description: mostra categorie
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/get_category', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        config.logger.info("get_category - Staff",staff)
        categoryDish(req, res)
    }
});

/**
 * @param {req,res} '\get_role'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di get role: ottenere il ruolo role staff
 */
/**
 * @swagger
 * /get role:
 *  get:
 *    description: ottieni ruoli
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/get_role', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        config.logger.info("get_role - Staff",staff)
        getAllRole(req, res)
    }
});

/**
 * @param {req,res} '\show_dish'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di show dish: mostra piatto role staff
 */
/**
 * @swagger
 * /show_dish:
 *  get:
 *    description: mostra piatti
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/show_dish', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        config.logger.info("show_dish - Staff",staff)
        showDish(req, res)
    }
});

/**
 * @param {req,res} '\new_table'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di new_table: crea un nuovo piatto
 */
/**
 * @swagger
 * /new table:
 *  post:
 *    description: crea nuovo tavolo
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/new_table', function(req, res) {
    staff = isStaff(req, res)
    if (staff) {
        config.logger.info("new_table - Staff",staff)
        createTable(req, res, staff)
    }
});

/**
 * @param {req,res} '\get_dish_by_uuid'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di get_dish_by_uuid: ritorna in output un piatto passando in input un uuid
 */
/**
 * @swagger
 * /get dish by uuid:
 *  get:
 *    description: ottieni un piatto passando un uuid
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/get_dish_by_uuid', function(req, res) {
    //staff = isAdminStaff(req, res)
    //if (staff) {
        getDish(req, res, staff)
    //}
});

/**
 * @param {req,res} '\get_all_dishes'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di get_all_dishes: ritorna tutti i piatti
 */
/**
 * @swagger
 * /get all dishes:
 *  get:
 *    description: ottieni tutti i piatti
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/get_all_dishes', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        config.logger.info("get_all_dishes - Staff",staff)
        getAllDishes(req, res, staff)
    }
});

/**
 * @param {req,res} '\new_category'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * funzione di new_category: crea una nuova categoria
 */
/**
 * @swagger
 * /new category:
 *  post:
 *    description: crea una nuova categoria
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/new_category', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        config.logger.info("new_category - Staff",staff)
        newCategory(req, res, staff)
    }
});

module.exports = router;