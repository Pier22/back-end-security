const express = require("express");
const fs = require("fs");
var cors = require("cors");
const app = express();
const helmet = require("helmet");
app.use(cors());
app.use(helmet());
app.use(helmet.hidePoweredBy())
app.use(helmet.xssFilter());
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const config = require("./config");
const { tableControl } = require("./controllers/occupy_function");
const toobusy = require('toobusy-js');
const clientsRoutes = require("./routes/client_routes");
const staffRoutes = require("./routes/staff_routes");
const adminRoutes = require("./routes/admin_routes");

// evitare i json troppo pesanti
app.use(express.urlencoded({ extended: true, limit: "3kb" }));
app.use(express.json({ limit: "3kb" }));


// Extended: https://swagger.io/specification/#infoObject

/**@summary Il nostro Progetto e' intitolato UniBookBar e prevede la realizzazione di una piattaforma online per la gestione del bar 
 * Universitario, l'obbiettivo principale del nostro applicativo online è quello di diminuire le code e l'affollamento nelle ore di 
 * punta davanti al bar dell'università. **/

const swaggerOptions = {

    swaggerDefinition: {

        info: {

            version: "1.0.0",

            title: "API",

            description: "GENERAL",

            contact: {

                name: "Amazing Developer"

            },

            servers: ["http://localhost:3000"]

        }

    },

    // ['.routes/*.js']

    apis: ["./routes/*"]

};

// evitare i dos attack con troppe chiamate al server in ogni API
app.use(function(req, res, next) {
    if (toobusy()) {
        // per mitigare i DoS attack
        res.send(503, "Server Too Busy");
    } else {
    next();
    }
});

const swaggerDocs = swaggerJsDoc(swaggerOptions);

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, PUT, PATCH, DELETE"
    );
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
    next();
});

// RIGHT WAY TO WRITE A SECURE CORS
//const srv = http.createServer((req, res) => {
   // res.writeHead(200, { 'Access-Control-Allow-Origin': '*' }); // Sensitive
   // res.end('ok');
 // });

setInterval(tableControl, 86400000);

app.use("/clients", clientsRoutes);
app.use("/staff", staffRoutes);
app.use("/admin", adminRoutes);



app.listen(3000, () => config.logger.info("listening on 3000"));

module.exports = app;